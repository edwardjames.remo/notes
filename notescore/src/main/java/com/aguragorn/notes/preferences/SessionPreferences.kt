package com.aguragorn.notes.preferences

interface SessionPreferences {
    var loggedInUserId: String
    var loggedInUserEmail: String
}