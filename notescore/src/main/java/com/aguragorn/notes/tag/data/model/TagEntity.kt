package com.aguragorn.notes.tag.data.model

import com.aguragorn.notes.data.model.Storable
import com.aguragorn.notes.domain.model.ModelConverter
import com.aguragorn.notes.tag.domain.model.Tag
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class TagEntity(
    @PrimaryKey
    override var id: String? = null,
    var name: String = ""
) : Storable<Tag>, RealmObject() {
    override fun getConverter() = TagEntityConverter
}

object TagEntityConverter : ModelConverter<Tag, Storable<Tag>> {
    override fun from(model: Tag): Storable<Tag> = TagEntity(
        id = model.id,
        name = model.name
    )

    override fun from(obj: Storable<Tag>): Tag {
        if (obj !is TagEntity) throw IllegalArgumentException(
            "obj ${obj::class.java.simpleName} is not a ${TagEntity::class.java.simpleName}"
        )

        return Tag(
            id = obj.id,
            name = obj.name
        )
    }

}