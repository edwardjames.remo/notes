package com.aguragorn.notes.tag.domain.model

import com.aguragorn.notes.domain.model.Model

class Tag(
    override var id: String? = null,
    var name: String = ""
) : Model