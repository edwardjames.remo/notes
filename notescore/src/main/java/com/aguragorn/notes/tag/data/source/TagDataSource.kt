package com.aguragorn.notes.tag.data.source

import com.aguragorn.notes.data.source.DataSourceType
import com.aguragorn.notes.tag.domain.model.Tag

interface TagDataSource : DataSourceType<Tag>