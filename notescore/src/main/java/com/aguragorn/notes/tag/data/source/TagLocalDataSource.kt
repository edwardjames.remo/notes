package com.aguragorn.notes.tag.data.source

import com.aguragorn.common.async.Async
import com.aguragorn.common.di.Provider
import com.aguragorn.notes.data.source.StorageContext
import com.aguragorn.notes.tag.data.model.TagEntity
import com.aguragorn.notes.tag.domain.model.Tag

class TagLocalDataSource(
    private val storageContext: StorageContext = Provider.singleton()
) : TagDataSource {
    override fun getAllAsync() = Async<List<Tag>>().execute {
        try {
            resolve(storageContext.fetch<TagEntity, Tag>().toList())
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }
}