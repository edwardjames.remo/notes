package com.aguragorn.notes.tag.data.source

import com.aguragorn.common.di.Provider
import com.aguragorn.notes.di.CoreDI.LOCAL_TAG_DATA_SOURCE

class TagRepository(
    private val tagLocalDataSource: TagDataSource = Provider.singleton(LOCAL_TAG_DATA_SOURCE)
) : TagDataSource {
    override fun getAllAsync() = tagLocalDataSource.getAllAsync()
}