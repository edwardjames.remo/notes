package com.aguragorn.notes.notesharing.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.Promise
import com.aguragorn.common.async.resolve
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCaseNoPayload
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.note.data.source.NoteDataSource
import com.aguragorn.notes.notesharing.data.source.SharedNoteDataSource
import com.aguragorn.notes.notesharing.utils.TAG_NOTE_SHARING
import com.aguragorn.notes.notesharing.utils.toNote
import com.aguragorn.notes.preferences.SessionPreferences

class GetNotesSharedToMeUseCase(
    private val sessionPreferences: SessionPreferences = Provider.singleton(),
    private val sharedNoteDataSource: SharedNoteDataSource = Provider.singleton(),
    private val notesDataSource: NoteDataSource = Provider.singleton()
) : AsyncUseCaseNoPayload<Unit> {
    override fun execute(request: Unit): Promise<Unit> = Async<Unit>().execute {
        try {
            val recipient = sessionPreferences.loggedInUserEmail
            val sharedNotes = sharedNoteDataSource.getAllSharedToUserAsync(recipient).await()
            Log.d(TAG_NOTE_SHARING, "notes shared to $recipient: ${sharedNotes.count()}")

            notesDataSource.saveAllAsync(sharedNotes.map { it.toNote() }).await()
            sharedNoteDataSource.deleteAllAsync(sharedNotes)
            resolve()
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }
}