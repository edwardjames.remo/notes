package com.aguragorn.notes.notesharing.utils

import com.aguragorn.notes.note.domain.model.Note
import com.aguragorn.notes.notesharing.domain.model.SharedNote

fun Note.toSharedNote(fromUser: String, toUser: String) : SharedNote = SharedNote(
    title = title,
    description = description,
    toUser = toUser,
    fromUser = fromUser
)

fun SharedNote.toNote(): Note = Note(
    title = title,
    description = description
)

const val TAG_NOTE_SHARING = "NoteSharing"