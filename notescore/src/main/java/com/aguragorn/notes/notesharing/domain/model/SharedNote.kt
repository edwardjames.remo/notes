package com.aguragorn.notes.notesharing.domain.model

import com.aguragorn.notes.domain.model.Model

class SharedNote(
    override var id: String? = null,
    var title: String,
    var description: String = "",
    var toUser: String,
    var fromUser: String
): Model {
    companion object
}