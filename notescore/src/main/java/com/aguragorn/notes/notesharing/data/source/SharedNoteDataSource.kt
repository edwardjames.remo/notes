package com.aguragorn.notes.notesharing.data.source

import com.aguragorn.common.async.Promise
import com.aguragorn.notes.data.source.DataSourceType
import com.aguragorn.notes.notesharing.domain.model.SharedNote

interface SharedNoteDataSource: DataSourceType<SharedNote> {
    fun getAllSharedToUserAsync(toUser: String): Promise<List<SharedNote>>
}