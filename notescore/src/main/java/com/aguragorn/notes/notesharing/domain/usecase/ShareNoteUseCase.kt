package com.aguragorn.notes.notesharing.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.Promise
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCase
import com.aguragorn.common.domain.usecase.execute
import com.aguragorn.notes.note.domain.model.Note
import com.aguragorn.notes.notesharing.data.source.SharedNoteDataSource
import com.aguragorn.notes.notesharing.domain.model.SharedNote
import com.aguragorn.notes.notesharing.utils.toSharedNote
import com.aguragorn.notes.preferences.SessionPreferences
import com.aguragorn.notes.user.InvalidUserAuthenticationException
import com.aguragorn.notes.user.domain.usecase.GetLoggedInUserUseCase

class ShareNoteUseCase(
    private val sessionPreferences: SessionPreferences = Provider.singleton(),
    private val sharedNoteDataSource: SharedNoteDataSource = Provider.singleton()
) : AsyncUseCase<ShareNoteUseCase.Request, ShareNoteUseCase.Response> {

    override fun execute(request: Request): Promise<Response> = Async<Response>().execute executeAsync@{
        try {
            val sender = sessionPreferences.loggedInUserEmail

            if (sender.isEmpty()) {
                rejectWith(InvalidUserAuthenticationException())
                return@executeAsync
            }

            val sharedNote = request.note.toSharedNote(fromUser = sender, toUser = request.toUser)
            sharedNoteDataSource.saveAsync(sharedNote).await()

            resolve(Response(sharedNote))
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    class Request(val note: Note, val toUser: String)
    class Response(val sharedNote: SharedNote)
}