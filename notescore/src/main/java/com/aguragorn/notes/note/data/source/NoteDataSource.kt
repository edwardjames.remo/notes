package com.aguragorn.notes.note.data.source

import com.aguragorn.notes.data.source.DataSourceType
import com.aguragorn.notes.note.domain.model.Note

interface NoteDataSource : DataSourceType<Note>