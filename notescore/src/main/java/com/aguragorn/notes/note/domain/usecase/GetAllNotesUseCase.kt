package com.aguragorn.notes.note.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCase
import com.aguragorn.common.domain.usecase.UseCase
import com.aguragorn.notes.note.data.source.NoteDataSource
import com.aguragorn.notes.note.domain.model.Note

class GetAllNotesUseCase(
    private val notesDataSource: NoteDataSource = Provider.singleton()
) : AsyncUseCase<Unit, GetAllNotesUseCase.Response> {

    override fun execute(request: Unit) = Async<Response>().execute {
        notesDataSource.getAllAsync()
            .onSuccess { notes ->
                resolve(Response(notes))
            }.onFailure {
                rejectWith(it)
            }
    }

    class Response(val notes: List<Note>)
}