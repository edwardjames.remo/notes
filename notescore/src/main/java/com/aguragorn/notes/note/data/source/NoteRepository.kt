package com.aguragorn.notes.note.data.source

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.Promise
import com.aguragorn.common.async.resolve
import com.aguragorn.common.di.Provider
import com.aguragorn.notes.di.CoreDI
import com.aguragorn.notes.note.domain.model.Note

class NoteRepository(
    private val localDataSource: NoteDataSource = Provider.singleton(CoreDI.LOCAL_NOTE_DATA_SOURCE),
    private val remoteDataSource: NoteDataSource = Provider.singleton(CoreDI.REMOTE_NOTE_DATA_SOURCE)
) : NoteDataSource {
    override fun getAllAsync() = Async<List<Note>>().execute {
        // watch for updates updates but don't wait
        localDataSource.getAllAsync()
            .onSuccess { resolve(it) }
            .onFailure { rejectWith(it) }
    }

    override fun saveAsync(data: Note) = Async<Note>().execute {
        try {
            val note = localDataSource.saveAsync(data).await()
            resolve(note)
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    override fun saveAllAsync(data: Collection<Note>): Promise<Collection<Note>> = Async<Collection<Note>>().execute {
        try {
            resolve(localDataSource.saveAllAsync(data).await())
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    override fun getByIdAsync(id: String): Promise<Note?> = localDataSource.getByIdAsync(id)

    override fun deleteAllAsync(data: List<Note>?) = Async<Unit>().execute {
        localDataSource.deleteAllAsync(data)
            .onSuccess { resolve() }
            .onFailure { rejectWith(it) }
    }
}