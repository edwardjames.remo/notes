package com.aguragorn.notes.note.domain.model

import com.aguragorn.notes.domain.model.Model
import com.aguragorn.notes.tag.domain.model.Tag

class Note(
    override var id: String? = null,
    var title: String,
    var description: String = "",
    var tags: MutableList<Tag> = mutableListOf()
) : Model {
    companion object
}