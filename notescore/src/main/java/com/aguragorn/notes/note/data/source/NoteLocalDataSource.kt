package com.aguragorn.notes.note.data.source

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.TaskQueue
import com.aguragorn.common.di.Provider
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.data.source.StorageContext
import com.aguragorn.notes.note.data.model.NoteEntity
import com.aguragorn.notes.note.data.model.NoteEntityConverter
import com.aguragorn.notes.note.domain.model.Note
import com.aguragorn.notes.notesharing.utils.TAG_NOTE_SHARING
import java.util.*

class NoteLocalDataSource(
    private val storageContext: StorageContext = Provider.singleton()
) : NoteDataSource {

    override fun saveAsync(data: Note) = Async<Note>().execute(TaskQueue.IO) {
        try {
            val entity = NoteEntityConverter.from(data)

            if (entity.id == null) {
                entity.id = UUID.randomUUID().toString()
                storageContext.create(entity)
            } else {
                storageContext.update(entity)
            }

            resolve(NoteEntityConverter.from(entity))
        } catch (e: Exception) {
            rejectWith(e)
        }
    }

    override fun saveAllAsync(data: Collection<Note>) = Async<Collection<Note>>().execute(TaskQueue.IO) {
        try {
            val forUpdate = data.asSequence()
                .filter { it.id != null }
                .map { NoteEntityConverter.from(it) }
                .toList()
            Log.d(TAG_NOTE_SHARING, "notes to update: ${forUpdate.count()}")

            val forCreation = data.asSequence()
                .filter { it.id == null }
                .map { NoteEntityConverter.from(it).apply { id = UUID.randomUUID().toString() } }
                .toList()

            Log.d(TAG_NOTE_SHARING, "notes to create: ${forCreation.count()}")

            storageContext.updateAll(forUpdate)
            storageContext.createAll(forCreation)

            resolve(
                mutableListOf<NoteEntity>().let { entities ->
                    entities.addAll(forCreation)
                    entities.addAll(forUpdate)
                    entities.map { NoteEntityConverter.from(it) }
                }
            )
        } catch (e: Exception) {
            rejectWith(e)
        }
    }

    override fun getByIdAsync(id: String) = Async<Note?>().execute(TaskQueue.IO) {
        try {
            resolve(
                storageContext
                    .fetch<NoteEntity, Note> { it.id == id }
                    .firstOrNull()
            )
        } catch (e: Exception) {
            rejectWith(e)
        }
    }

    override fun getAllAsync() = Async<List<Note>>().execute(TaskQueue.IO) {
        try {
            resolve(storageContext.fetch<NoteEntity, Note>().toList())
        } catch (e: Exception) {
            rejectWith(e)
        }
    }

    override fun deleteAllAsync(data: List<Note>?) = Async<Unit>().execute(TaskQueue.IO) {
        val entities = data?.map { NoteEntityConverter.from(it) }
        try {
            resolve(storageContext.deleteAll(entities))
        } catch (e: Exception) {
            rejectWith(e)
        }
    }
}