package com.aguragorn.notes.note.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCase
import com.aguragorn.common.domain.usecase.UseCase
import com.aguragorn.notes.note.data.source.NoteDataSource
import com.aguragorn.notes.note.domain.model.Note

class SaveNoteUseCase(
    private val noteRepository: NoteDataSource = Provider.singleton()
) : AsyncUseCase<SaveNoteUseCase.Request, SaveNoteUseCase.Response> {

    override fun execute(request: Request) = Async<Response>().execute {
        try {
            val note = noteRepository.saveAsync(request.note).await()
            resolve(Response(note))
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    class Request(val note: Note)
    class Response(val note: Note)
}