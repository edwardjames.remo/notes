package com.aguragorn.notes.note.data.model

import com.aguragorn.notes.data.model.Storable
import com.aguragorn.notes.data.source.realm.toRealmList
import com.aguragorn.notes.domain.model.ModelConverter
import com.aguragorn.notes.note.domain.model.Note
import com.aguragorn.notes.tag.data.model.TagEntity
import com.aguragorn.notes.tag.data.model.TagEntityConverter
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class NoteEntity(
    @PrimaryKey
    override var id: String? = null,
    var title: String = "",
    var description: String = "",
    var tags: RealmList<TagEntity> = RealmList()
) : Storable<Note>, RealmObject() {
    override fun getConverter() = NoteEntityConverter
}

object NoteEntityConverter : ModelConverter<Note, Storable<Note>> {
    override fun from(model: Note) = NoteEntity(
        id = model.id,
        title = model.title,
        description = model.description,
        tags = model.tags.map { TagEntityConverter.from(it) as TagEntity }.toRealmList()
    )

    override fun from(obj: Storable<Note>): Note {
        if (obj !is NoteEntity) throw IllegalArgumentException(
            "obj ${obj::class.java.simpleName} is not a ${NoteEntity::class.java.simpleName}"
        )

        return Note(
            id = obj.id,
            title = obj.title,
            description = obj.description,
            tags = obj.tags.map { TagEntityConverter.from(it) }.toMutableList()
        )
    }
}