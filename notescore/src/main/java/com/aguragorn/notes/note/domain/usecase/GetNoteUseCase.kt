package com.aguragorn.notes.note.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCase
import com.aguragorn.common.domain.usecase.UseCase
import com.aguragorn.notes.note.data.source.NoteDataSource
import com.aguragorn.notes.note.domain.model.Note

class GetNoteUseCase(
    private val noteRepository: NoteDataSource = Provider.singleton()
) : AsyncUseCase<GetNoteUseCase.Request, GetNoteUseCase.Response> {
    override fun execute(request: Request) = Async<Response>().execute {
        try {
            when {
                request.id?.isNotBlank() == true -> {
                    val note = noteRepository.getByIdAsync(request.id).await()
                    resolve(Response(note))
                }
            }
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    class Request(val id: String?)
    class Response(val note: Note?)
}