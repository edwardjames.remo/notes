package com.aguragorn.notes.note.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.resolve
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCase
import com.aguragorn.common.domain.usecase.UseCase
import com.aguragorn.notes.note.data.source.NoteDataSource
import com.aguragorn.notes.note.domain.model.Note

class DeleteNoteUseCase(
    private val noteRepository: NoteDataSource = Provider.singleton()
) : AsyncUseCase<DeleteNoteUseCase.Request, Unit> {

    override fun execute(request: Request) = Async<Unit>().execute {
        try {
            noteRepository.deleteAllAsync(listOf(request.note)).await()
            resolve()
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    class Request(val note: Note)
}