package com.aguragorn.notes.data.source

import android.content.Context
import com.aguragorn.notes.data.model.Storable
import com.aguragorn.notes.domain.model.Model

class SortType<T, R : Comparable<R>>(
    val selector: (T) -> R?,
    val ascending: Boolean = true
)

sealed class ConfigurationType(val name: String) {
    class Basic(name: String, val directory: String? = null) : ConfigurationType(name)
    class InMemory(name: String) : ConfigurationType(name)
}

abstract class StorageContext(
    protected val appContext: Context
) {
    abstract fun init()

    abstract fun <T : Storable<ModelType>, ModelType : Model> create(entity: T, clazz: Class<T>): ModelType
    inline fun <reified T : Storable<ModelType>, ModelType : Model> create(entity: T) = create(entity, T::class.java)

    abstract fun <T : Storable<ModelType>, ModelType : Model> createAll(
        entities: Collection<T>,
        clazz: Class<T>
    ): Collection<ModelType>

    inline fun <reified T : Storable<ModelType>, ModelType : Model> createAll(entities: List<T>) =
        createAll(entities, T::class.java)

    abstract fun <T : Storable<ModelType>, ModelType : Model> update(entity: T, clazz: Class<T>)
    inline fun <reified T : Storable<ModelType>, ModelType : Model> update(entity: T) = update(entity, T::class.java)

    abstract fun <T : Storable<ModelType>, ModelType : Model> updateAll(entities: Collection<T>, clazz: Class<T>)
    inline fun <reified T : Storable<ModelType>, ModelType : Model> updateAll(entities: List<T>) =
        updateAll(entities, T::class.java)

    abstract fun <T : Storable<ModelType>, ModelType : Model> delete(entity: T, clazz: Class<T>)
    inline fun <reified T : Storable<ModelType>, ModelType : Model> delete(entity: T) = delete(entity, T::class.java)

    abstract fun <T : Storable<ModelType>, ModelType : Model> deleteAll(entities: List<T>? = null, clazz: Class<T>)
    inline fun <reified T : Storable<ModelType>, ModelType : Model> deleteAll(entities: List<T>? = null) =
        deleteAll(entities, T::class.java)

    abstract fun <T : Storable<ModelType>, ModelType : Model> fetch(clazz: Class<T>): Collection<ModelType>
    inline fun <reified T : Storable<ModelType>, ModelType : Model, R : Comparable<R>> fetch(
        noinline predicate: ((ModelType) -> Boolean)? = null,
        sortBy: SortType<ModelType, R>
    ): Collection<ModelType> = fetch(T::class.java).asSequence().apply {
        predicate?.let { filter(it) }
        if (sortBy.ascending) {
            sortedBy(sortBy.selector)
        } else {
            sortedByDescending(sortBy.selector)
        }
    }.toList()

    inline fun <reified T : Storable<ModelType>, ModelType : Model> fetch(
        noinline predicate: ((ModelType) -> Boolean)? = null
    ): Collection<ModelType> = fetch(T::class.java).let { result ->
        predicate?.let { result.filter(it) } ?: result
    }
}