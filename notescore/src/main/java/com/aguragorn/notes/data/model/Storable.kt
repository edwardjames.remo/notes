package com.aguragorn.notes.data.model

import com.aguragorn.notes.domain.model.Model
import com.aguragorn.notes.domain.model.ModelConvertible
import io.realm.RealmModel

/**
 * All objects that will be stored in the data source must extend this class.
 * TODO: Move everything Realm related in this class when inheritance is already supported.
 */
interface Storable<ModelType : Model> : Model, ModelConvertible<ModelType, Storable<ModelType>>, RealmModel