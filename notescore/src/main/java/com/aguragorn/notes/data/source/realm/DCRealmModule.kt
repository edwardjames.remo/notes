package com.aguragorn.notes.data.source.realm

import com.aguragorn.notes.note.data.model.NoteEntity
import com.aguragorn.notes.tag.data.model.TagEntity
import io.realm.annotations.RealmModule

@RealmModule(
    classes = [
        NoteEntity::class,
        TagEntity::class
    ]
)
object DCRealmModule