package com.aguragorn.notes.data.source

class DataSourceException(msg: String) : Exception(msg)