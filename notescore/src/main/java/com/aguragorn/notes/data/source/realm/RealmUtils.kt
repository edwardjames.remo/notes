package com.aguragorn.notes.data.source.realm

import io.realm.RealmList

inline fun <reified T> Collection<T>.toRealmList(): RealmList<T> = RealmList(*this.toTypedArray())