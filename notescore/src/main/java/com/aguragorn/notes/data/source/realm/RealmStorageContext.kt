package com.aguragorn.notes.data.source.realm

import android.content.Context
import com.aguragorn.common.di.Provider
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.data.model.Storable
import com.aguragorn.notes.data.source.ConfigurationType
import com.aguragorn.notes.data.source.DataSourceException
import com.aguragorn.notes.data.source.StorageContext
import com.aguragorn.notes.domain.model.Model
import com.aguragorn.notes.preferences.SessionPreferences
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmObject
import java.io.File

class RealmStorageContext(
    private val sessionPreferences: SessionPreferences = Provider.singleton(),
    appContext: Context = Provider.singleton()
) : StorageContext(appContext.applicationContext) {

    // always create a new instance since Realm instances are always closed after use.
    private val realm: Realm
        get() {
            val configBuilder = RealmConfiguration.Builder().apply {
                val config = ConfigurationType.Basic(sessionPreferences.loggedInUserId)

                name("${config.name}.realm")
                modules(DCRealmModule)
                val databasePath = appContext.getDatabasePath("db").parentFile

                if (config.directory != null) {
                    val realmDirectory = File(config.directory).relativeTo(databasePath)
                    directory(realmDirectory)
                } else {
                    directory(databasePath)
                }
            }

            val config = configBuilder.build()
            Log.d(TAG_DATABASE, "using realm: ${config.realmFileName}")
            return Realm.getInstance(config)
        }

    override fun init() {
        Realm.init(appContext)
    }

    override fun <T : Storable<ModelType>, ModelType : Model> create(entity: T, clazz: Class<T>): ModelType =
        realm.useForTransaction {
            val storable = it.copyToRealm(entity)
            storable.getConverter().from(storable)
        } ?: throw DataSourceException("Unknown entity creation result")

    override fun <T : Storable<ModelType>, ModelType : Model> createAll(
        entities: Collection<T>,
        clazz: Class<T>
    ): Collection<ModelType> =
        realm.useForTransaction {
            it.copyToRealm(entities.toMutableList()).map { storable -> storable.getConverter().from(storable) }
        } ?: throw DataSourceException("Unknown entity creation result")

    override fun <T : Storable<ModelType>, ModelType : Model> update(entity: T, clazz: Class<T>) {
        realm.useForTransaction { it.copyToRealmOrUpdate(entity) }
    }

    override fun <T : Storable<ModelType>, ModelType : Model> updateAll(entities: Collection<T>, clazz: Class<T>) {
        realm.useForTransaction { it.copyToRealmOrUpdate(entities.toMutableList()) }
    }

    override fun <T : Storable<ModelType>, ModelType : Model> delete(entity: T, clazz: Class<T>) {
        realm.useForTransaction { realm ->
            realm.where(clazz).equalTo(Storable<ModelType>::id.name, entity.id).findFirst()?.also {
                RealmObject.deleteFromRealm(it)
            }
        }
    }

    override fun <T : Storable<ModelType>, ModelType : Model> deleteAll(entities: List<T>?, clazz: Class<T>) {
        if (entities == null) {
            realm.useForTransaction { it.delete(clazz) }
        } else {
            realm.useForTransaction { realm ->
                realm.where(clazz).`in`(Storable<ModelType>::id.name, entities.map { it.id }.toTypedArray())
                    .findAll()
                    .deleteAllFromRealm()
            }
        }
    }

    override fun <T : Storable<ModelType>, ModelType : Model> fetch(
        clazz: Class<T>
    ): Collection<ModelType> = realm.use { realm ->
        realm.where(clazz).findAll().asSequence().toList().map { storable -> storable.getConverter().from(storable) }
    }
}

/**
 * Executes the action inside a realm transaction and automatically closes the realm
 */
fun <T> Realm.useForTransaction(action: (Realm) -> T?): T? {
    var result: T? = null
    this.use { realm ->
        realm.executeTransaction {
            result = action(it)
        }
    }
    return result
}

val TAG_DATABASE = "database"