package com.aguragorn.notes.data.source

import com.aguragorn.common.async.Promise
import com.aguragorn.common.utils.NotImplementedException
import com.aguragorn.notes.domain.model.Model

    interface DataSourceType<T : Model> {
        fun saveAsync(data: T): Promise<T> {
            throw NotImplementedException()
        }

        fun saveAllAsync(data: Collection<T>): Promise<Collection<T>> {
            throw NotImplementedException()
        }

        /**
         * @return [Model] with the same [Model.id] as [id]. `null` if none matches.
         */
        fun getByIdAsync(id: String): Promise<T?> {
            throw NotImplementedException()
        }

        fun getAllAsync(): Promise<List<T>> {
            throw NotImplementedException()
        }

        /**
         * @param data the [Model]s to delete. If not provided all entries in the
         * data source are removed
         */
        fun deleteAllAsync(data: List<T>? = null): Promise<Unit> {
            throw NotImplementedException()
        }
    }