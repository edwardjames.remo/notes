package com.aguragorn.notes.di

import android.content.Context
import com.aguragorn.common.di.Provider
import com.aguragorn.notes.data.source.StorageContext
import com.aguragorn.notes.data.source.realm.RealmStorageContext
import com.aguragorn.notes.note.data.source.NoteDataSource
import com.aguragorn.notes.note.data.source.NoteLocalDataSource
import com.aguragorn.notes.note.data.source.NoteRemoteDataSource
import com.aguragorn.notes.note.data.source.NoteRepository
import com.aguragorn.notes.note.domain.usecase.DeleteNoteUseCase
import com.aguragorn.notes.note.domain.usecase.GetAllNotesUseCase
import com.aguragorn.notes.note.domain.usecase.GetNoteUseCase
import com.aguragorn.notes.note.domain.usecase.SaveNoteUseCase
import com.aguragorn.notes.notesharing.domain.usecase.GetNotesSharedToMeUseCase
import com.aguragorn.notes.notesharing.domain.usecase.ShareNoteUseCase
import com.aguragorn.notes.tag.data.source.TagDataSource
import com.aguragorn.notes.tag.data.source.TagLocalDataSource
import com.aguragorn.notes.tag.data.source.TagRepository
import com.aguragorn.notes.user.domain.usecase.GetLoggedInUserUseCase

object CoreDI {
    const val REMOTE_NOTE_DATA_SOURCE = "com.honorsoft.notes.core.RemoteNoteDataSource"
    const val LOCAL_NOTE_DATA_SOURCE = "com.honorsoft.notes.core.LocalNoteDataSource"
    const val LOCAL_TAG_DATA_SOURCE = "com.honorsoft.notes.core.LocalTagDataSource"

    @Suppress("UNUSED_PARAMETER")
    fun setup(context: Context) {

        Provider.register { RealmStorageContext() as StorageContext }

        Provider.register { NoteRepository() as NoteDataSource }
        Provider.register(REMOTE_NOTE_DATA_SOURCE) { NoteRemoteDataSource() as NoteDataSource }
        Provider.register(LOCAL_NOTE_DATA_SOURCE) { NoteLocalDataSource() as NoteDataSource }

        Provider.register { TagRepository() as TagDataSource }
        Provider.register(LOCAL_TAG_DATA_SOURCE) { TagLocalDataSource() as TagDataSource }

        Provider.register { SaveNoteUseCase() }
        Provider.register { GetAllNotesUseCase() }
        Provider.register { GetNoteUseCase() }
        Provider.register { DeleteNoteUseCase() }
        Provider.register { GetLoggedInUserUseCase() }
        Provider.register { ShareNoteUseCase() }
        Provider.register { GetNotesSharedToMeUseCase() }
    }
}