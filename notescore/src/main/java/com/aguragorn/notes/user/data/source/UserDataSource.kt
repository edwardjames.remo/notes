package com.aguragorn.notes.user.data.source

import com.aguragorn.common.async.Promise
import com.aguragorn.notes.user.domain.model.User

interface UserDataSource {
    fun getLoggedInUser(): Promise<User?>
}