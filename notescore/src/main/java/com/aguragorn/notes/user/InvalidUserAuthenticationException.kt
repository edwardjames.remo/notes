package com.aguragorn.notes.user

class InvalidUserAuthenticationException : Exception("No logged-in user found.")