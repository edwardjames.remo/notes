package com.aguragorn.notes.user.domain.usecase

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.Promise
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.AsyncUseCaseNoPayload
import com.aguragorn.notes.user.data.source.UserDataSource
import com.aguragorn.notes.user.domain.model.User

class GetLoggedInUserUseCase(
    private val userDataSource: UserDataSource = Provider.singleton()
) : AsyncUseCaseNoPayload<User?> {

    override fun execute(request: Unit): Promise<User?> = Async<User?>().execute {
        resolve(userDataSource.getLoggedInUser().await())
    }

}