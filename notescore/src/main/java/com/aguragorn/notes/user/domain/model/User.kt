package com.aguragorn.notes.user.domain.model

class User(
    var id: String? = null,
    var username: String,
    var password: String? = null,
    var email: String
)