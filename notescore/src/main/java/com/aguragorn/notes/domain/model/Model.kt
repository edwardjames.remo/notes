package com.aguragorn.notes.domain.model

interface Model {
    var id: String?
}

interface ModelConverter<ModelType : Model, ObjType> {
    fun from(model: ModelType): ObjType
    fun from(obj: ObjType): ModelType
}

interface ModelConvertible<ModelType : Model, ObjType> {
    fun getConverter(): ModelConverter<ModelType, ObjType>
}