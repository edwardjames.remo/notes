package com.aguragorn.notes.noteeditor.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.aguragorn.common.async.AsyncFragment
import com.aguragorn.common.lifecycle.getViewModel
import com.aguragorn.common.lifecycle.inflate
import com.aguragorn.common.livedata.addHandler
import com.aguragorn.commonui.app.actionBar
import com.aguragorn.commonui.app.setActionBar
import com.aguragorn.notes.R
import com.aguragorn.notes.databinding.NoteEditorFragmentBinding
import com.aguragorn.notes.databinding.NoteEditorTagItemBinding
import com.aguragorn.notes.noteeditor.presentation.model.NoteEditorEvent
import com.aguragorn.notes.noteeditor.presentation.model.NoteUIModel
import com.yarolegovich.lovelydialog.LovelyTextInputDialog
import kotlinx.android.synthetic.main.note_editor_fragment.*
import kotlinx.android.synthetic.main.note_editor_tag_item.view.*


class NoteEditorFragment : AsyncFragment() {

    private val noteEditorController by lazy { getViewModel<NoteEditorController>() }
    private val args: NoteEditorFragmentArgs by navArgs()

    // region fragment callbacks
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = inflate<NoteEditorFragmentBinding>(inflater, R.layout.note_editor_fragment, container)
        binding.noteEditorController = noteEditorController
        return binding.root
    }

    override suspend fun asyncOnViewCreated(view: View, savedInstanceState: Bundle?) {
        setActionBar(noteEditorToolbar)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowTitleEnabled(false)
        setHasOptionsMenu(true)

        noteEditorController.event.addHandler(this, ::handleControllerEvents)
        noteEditorController.currentNote.observe(this, Observer { onNoteLoaded(it) })
    }

    override suspend fun asyncOnStart() {
        val args = this.args
        if (args.noteId.isNullOrBlank()) {
            noteEditorController.createNewNote()
        } else {
            noteEditorController.editNote(args.noteId)
        }
    }

    override suspend fun asyncOnStop() {
        noteEditorController.onNoteEditorClosed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> noteEditorController.onNavigateBack()
        }

        return true
    }
    // endregion

    // region private methods
    private fun handleControllerEvents(noteEditorEvent: NoteEditorEvent) {
        when (noteEditorEvent) {
            is NoteEditorEvent.CloseEditor -> view?.findNavController()?.navigateUp()
            is NoteEditorEvent.ShowSharingDialog -> showSharingDialog()
        }
    }

    private fun showSharingDialog() {
        if (!isVisible || isRemoving) return

        LovelyTextInputDialog(context)
            .setTopColorRes(R.color.colorAccent)
            .setIcon(R.drawable.ic_note_editor_share_dialog_avatar)
            .setConfirmButton(R.string.note_editor_share_dialog_confirm_label) {
                noteEditorController.onRecipientConfirmed(it)
            }
            .show()
    }

    private fun onNoteLoaded(note: NoteUIModel?) {
        val tags = note?.tags?.value
        if (tags.isNullOrEmpty()) return

        tags.forEach { tag ->
            noteEditorTagGrp.apply {
                val binding = inflate<NoteEditorTagItemBinding>(R.layout.note_editor_tag_item)
                binding.tag = tag
                binding.root.noteEditorTagBtn
                    .setOnCloseIconClickListener { noteEditorController.onRemoveTagClicked(tag) }

                addView(binding.root)
            }
        }
    }
    // endregion
}
