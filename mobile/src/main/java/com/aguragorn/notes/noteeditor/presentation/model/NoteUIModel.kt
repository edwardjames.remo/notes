package com.aguragorn.notes.noteeditor.presentation.model

import androidx.lifecycle.MutableLiveData
import com.aguragorn.common.collection.isSameAs
import com.aguragorn.common.livedata.*
import com.aguragorn.notes.note.domain.model.Note

class NoteUIModel(
    var id: String? = null,
    val title: MutableLiveData<String> = mutableLiveDataOf(),
    val description: MutableLiveData<String> = mutableLiveDataOf(),
    val tags: MutableLiveList<TagUIModel> = mutableLiveListOf(),
    var deleted: Boolean = false
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is NoteUIModel) return false

        if (id != other.id) return false
        if (title.value != other.title.value) return false
        if (description.value != other.description.value) return false
        if (!tags.value.isSameAs(other.tags.value)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + title.value.hashCode()
        result = 31 * result + description.value.hashCode()
        return result
    }

    companion object
}

fun NoteUIModel.Companion.from(note: Note) = NoteUIModel(
    id = note.id,
    title = mutableLiveDataOf(note.title),
    description = mutableLiveDataOf(note.description),
    tags = note.tags.map { TagUIModel.from(it) }.toMutableLiveList()
)

fun NoteUIModel.toNote() = Note(
    id = id,
    title = title.value.orEmpty(),
    description = description.value.orEmpty(),
    tags = tags.value.orEmpty().map { it.toTag() }.toMutableList()
)

fun NoteUIModel.copy() = NoteUIModel(
    id = id,
    title = title.copy(),
    description = description.copy(),
    tags = tags.copy()
)

