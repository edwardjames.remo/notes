package com.aguragorn.notes.noteeditor.presentation.model

import com.aguragorn.notes.tag.domain.model.Tag

class TagUIModel(
    var id: String? = null,
    var name: String = ""
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TagUIModel) return false

        if (id != other.id) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + name.hashCode()
        return result
    }

    companion object
}

fun TagUIModel.Companion.from(tag: Tag) = TagUIModel(
    id = tag.id,
    name = tag.name
)

fun TagUIModel.toTag() = Tag(id = id, name = name)