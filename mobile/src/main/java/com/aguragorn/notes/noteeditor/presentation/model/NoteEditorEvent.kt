package com.aguragorn.notes.noteeditor.presentation.model

import com.aguragorn.common.livedata.Event

sealed class NoteEditorEvent : Event {
    object CloseEditor : NoteEditorEvent()
    object OpenTagEditor : NoteEditorEvent()
    object ShowSharingDialog : NoteEditorEvent()
}