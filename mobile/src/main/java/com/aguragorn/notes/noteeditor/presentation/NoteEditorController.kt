package com.aguragorn.notes.noteeditor.presentation

import androidx.lifecycle.ViewModel
import com.aguragorn.common.async.TaskQueue
import com.aguragorn.common.di.Provider
import com.aguragorn.common.livedata.mutableLiveDataOf
import com.aguragorn.common.livedata.removeAll
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.note.domain.usecase.DeleteNoteUseCase
import com.aguragorn.notes.note.domain.usecase.GetNoteUseCase
import com.aguragorn.notes.note.domain.usecase.SaveNoteUseCase
import com.aguragorn.notes.noteeditor.presentation.model.*
import com.aguragorn.notes.notesharing.domain.usecase.ShareNoteUseCase

class NoteEditorController(
    private val saveNoteUseCase: SaveNoteUseCase = Provider.singleton(),
    private val getNoteUseCase: GetNoteUseCase = Provider.singleton(),
    private val deleteNoteUseCase: DeleteNoteUseCase = Provider.singleton(),
    private val shareNoteUseCase: ShareNoteUseCase = Provider.singleton()
) : ViewModel() {
    private var originalNote: NoteUIModel? = null
    val currentNote = mutableLiveDataOf<NoteUIModel>()
    val event = mutableLiveDataOf<NoteEditorEvent>()

    fun createNewNote() {
        currentNote.value = NoteUIModel()
        originalNote = currentNote.value?.copy()
    }

    suspend fun editNote(noteId: String) {
        val response = getNoteUseCase.execute(GetNoteUseCase.Request(noteId)).await()

        val note = response.note ?: run {
            createNewNote()
            return
        }

        currentNote.value = NoteUIModel.from(note)
    }

    fun onNavigateBack() {
        TaskQueue.runUI {
            event.value = NoteEditorEvent.CloseEditor
        }
    }

    suspend fun onNoteEditorClosed() {
        try {
            when {
                currentNote.value?.deleted == true -> {
                    currentNote.value?.toNote()?.let {
                        deleteNoteUseCase.execute(DeleteNoteUseCase.Request(it)).await()
                    }
                }
                // TODO: Add validation of empty notes
                currentNote.value != originalNote -> {
                    currentNote.value?.toNote()?.let {
                        saveNoteUseCase.execute(SaveNoteUseCase.Request(it)).await()
                    }
                }
            }
        } catch (e: Throwable) {
            Log.e("TAG", "error in closing editor", e)
        }

        // Blank-out the note details
        createNewNote()
    }

    fun onNoteDeleteClicked() {
        currentNote.value?.deleted = true
        event.value = NoteEditorEvent.CloseEditor
    }

    fun onShareNoteClicked() {
        if (currentNote.value == null) return
        event.value = NoteEditorEvent.ShowSharingDialog
    }

    fun onRemoveTagClicked(tag: TagUIModel) {
        currentNote.value?.tags?.removeAll(listOf(tag))
    }

    fun onCreateTagClicked() {
        event.value = NoteEditorEvent.OpenTagEditor
    }

    fun onRecipientConfirmed(toUser: String?) {
        val currentNote = currentNote.value ?: return
        if (toUser.isNullOrBlank()) return

        shareNoteUseCase.execute(
            ShareNoteUseCase.Request(currentNote.toNote(), toUser)
        )
    }
}