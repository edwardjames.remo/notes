package com.aguragorn.notes.di

import android.content.Context
import com.aguragorn.common.di.Provider
import com.aguragorn.notes.notesharing.data.source.SharedNoteDataSource
import com.aguragorn.notes.notesharing.data.source.SharedNoteParseDB
import com.aguragorn.notes.preferences.MobileSessionPreferences
import com.aguragorn.notes.preferences.PreferenceManager
import com.aguragorn.notes.preferences.SessionPreferences
import com.aguragorn.notes.user.data.source.UserDataSource
import com.aguragorn.notes.user.data.source.UserParseDB

object MobileDI {

    fun setup(context: Context) {
        Provider.register { context.applicationContext as Context }

        Provider.register { UserParseDB() as UserDataSource }
        Provider.register { PreferenceManager() }
        Provider.register { MobileSessionPreferences as SessionPreferences }

        Provider.register { SharedNoteParseDB() as SharedNoteDataSource }

    }
}