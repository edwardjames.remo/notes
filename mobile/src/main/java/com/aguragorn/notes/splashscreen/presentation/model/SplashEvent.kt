package com.aguragorn.notes.splashscreen.presentation.model

import com.aguragorn.common.livedata.Event

sealed class SplashEvent : Event {
    object ShowLoginScreen : SplashEvent()
    object ShowNoteListScreen : SplashEvent()
    object StartSharedNotesPolling: SplashEvent()
}