package com.aguragorn.notes.splashscreen.presentation

import androidx.lifecycle.ViewModel
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.execute
import com.aguragorn.common.livedata.mutableLiveDataOf
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.preferences.SessionPreferences
import com.aguragorn.notes.splashscreen.presentation.model.SplashEvent
import com.aguragorn.notes.user.domain.model.User
import com.aguragorn.notes.user.domain.usecase.GetLoggedInUserUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class SplashScreenController(
    private val getLoggedInUserUseCase: GetLoggedInUserUseCase = Provider.singleton(),
    private val sessionPreferences: SessionPreferences = Provider.singleton()
) : ViewModel() {

    val event = mutableLiveDataOf<SplashEvent>()

    suspend fun start() {
        evaluateLoggedInUser()
    }

    suspend fun onLoginCompleted() {
        evaluateLoggedInUser()
    }

    private suspend fun evaluateLoggedInUser() {
        val loggedInUser = getLoggedInUserUseCase.execute().await()

        if (loggedInUser == null) {
            event.value = SplashEvent.ShowLoginScreen
            sessionPreferences.loggedInUserId = ""
            sessionPreferences.loggedInUserEmail = ""
        } else {
            Log.i(TAG_AUTHENTICATION, "logging-in: ${loggedInUser.id}")

            loggedInUser.id?.let { id ->
                // use a different storage for
                sessionPreferences.loggedInUserId = id
                sessionPreferences.loggedInUserEmail = loggedInUser.email
                event.value = SplashEvent.StartSharedNotesPolling
                event.value = SplashEvent.ShowNoteListScreen
            }
        }
    }
}

val TAG_AUTHENTICATION get() = "authentication"