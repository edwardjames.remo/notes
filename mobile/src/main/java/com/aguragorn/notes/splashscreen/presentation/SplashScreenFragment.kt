package com.aguragorn.notes.splashscreen.presentation

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.aguragorn.common.async.AsyncFragment
import com.aguragorn.common.lifecycle.getViewModel
import com.aguragorn.common.livedata.addHandler
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.R
import com.aguragorn.notes.notesharing.service.SharedNotesPollingService
import com.aguragorn.notes.notesharing.utils.TAG_NOTE_SHARING
import com.aguragorn.notes.splashscreen.presentation.model.SplashEvent
import com.parse.ui.login.ParseLoginBuilder

private const val RC_LOGIN = 0

class SplashScreenFragment : AsyncFragment() {
    private val splashScreenController: SplashScreenController by lazy { getViewModel<SplashScreenController>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.splash_screen_fragment, container, false)
    }

    override suspend fun asyncOnViewCreated(view: View, savedInstanceState: Bundle?) {
        splashScreenController.event.addHandler(this, ::handleControllerEvents)
    }

    override suspend fun asyncOnStart() {
        splashScreenController.start()
    }

    override suspend fun asyncOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RC_LOGIN) splashScreenController.onLoginCompleted()
    }

    private fun handleControllerEvents(event: SplashEvent) {
        when (event) {
            is SplashEvent.ShowLoginScreen -> {
                startActivityForResult(
                    ParseLoginBuilder(context)
                        .setParseLoginEnabled(true)
                        .setParseSignupButtonText("Create an Account")
                        .setParseLoginEmailAsUsername(true)
                        .build(),
                    RC_LOGIN
                )
            }
            is SplashEvent.ShowNoteListScreen -> {
                val action = SplashScreenFragmentDirections.openNoteListFragment()
                view?.findNavController()?.navigate(action)
            }
            is SplashEvent.StartSharedNotesPolling -> {
                Log.d(TAG_NOTE_SHARING, "attempting to stat shared notes polling service.")
                SharedNotesPollingService.schedule()
            }
        }
    }
}
