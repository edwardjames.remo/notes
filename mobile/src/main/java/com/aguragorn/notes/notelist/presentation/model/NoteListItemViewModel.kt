package com.aguragorn.notes.notelist.presentation.model

import androidx.lifecycle.LiveData
import com.aguragorn.common.livedata.liveDataOf
import com.aguragorn.notes.note.domain.model.Note

class NoteListItemViewModel(
    val id: String,
    val title: LiveData<String> = liveDataOf()
) {
    companion object
}

fun NoteListItemViewModel.Companion.from(note: Note) = NoteListItemViewModel(
    id = note.id!!,
    title = liveDataOf(note.title)
)

