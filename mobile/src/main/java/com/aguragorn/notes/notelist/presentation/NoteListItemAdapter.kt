package com.aguragorn.notes.notelist.presentation

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aguragorn.common.lifecycle.inflate
import com.aguragorn.common.livedata.mutableLiveDataOf
import com.aguragorn.notes.R
import com.aguragorn.notes.databinding.NoteListItemBinding
import com.aguragorn.notes.notelist.presentation.model.NoteListItemEvent
import com.aguragorn.notes.notelist.presentation.model.NoteListItemViewModel

// TODO: Extract interface and base classes for ObservingAdapter
class NoteListItemAdapter :
    ItemObserver<NoteListItemViewModel>,
    RecyclerView.Adapter<NoteListItemAdapter.ViewHolder>() {

    val event = mutableLiveDataOf<NoteListItemEvent>()
    var items = mutableListOf<NoteListItemViewModel>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = parent.inflate<NoteListItemBinding>(R.layout.note_list_item)
        return ViewHolder(binding, this)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.note = items[position]
    }

    override fun onItemClicked(item: NoteListItemViewModel) {
        event.value = NoteListItemEvent.NoteSelectedEvent(item)
    }

    class ViewHolder(
        val binding: NoteListItemBinding,
        itemObserver: ItemObserver<NoteListItemViewModel>
    ) : RecyclerView.ViewHolder(binding.noteListItemRoot) {
        init {
            binding.itemObserver = itemObserver
        }
    }
}

interface ItemObserver<T : Any> {
    fun onItemClicked(item: T)
}