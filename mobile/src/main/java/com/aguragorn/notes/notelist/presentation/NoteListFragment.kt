package com.aguragorn.notes.notelist.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aguragorn.common.async.AsyncFragment
import com.aguragorn.common.lifecycle.getViewModel
import com.aguragorn.common.lifecycle.inflate
import com.aguragorn.common.livedata.addHandler
import com.aguragorn.notes.R
import com.aguragorn.notes.databinding.NoteListFragmentBinding
import com.aguragorn.notes.notelist.presentation.model.NoteListEvent
import com.aguragorn.notes.notelist.presentation.model.NoteListItemEvent
import com.aguragorn.notes.notelist.presentation.model.NoteListItemViewModel
import kotlinx.android.synthetic.main.note_list_fragment.*

class NoteListFragment : AsyncFragment() {

    private val noteListController: NoteListController by lazy { getViewModel<NoteListController>() }
    private val noteListItemAdapter = NoteListItemAdapter()

    // region fragment callbacks
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = inflate<NoteListFragmentBinding>(inflater, R.layout.note_list_fragment, container)
        binding.noteListController = noteListController
        return binding.root
    }

    override suspend fun asyncOnViewCreated(view: View, savedInstanceState: Bundle?) {
        noteListController.event.addHandler(this, ::handleControllerEvents)
        noteListController.noteList.observe(this, Observer { this.handleNoteListChanged(it) })

        val context = this.context ?: return

        noteListItemAdapter.event.addHandler(this, ::handleItemEvents)

        noteListNotes.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = noteListItemAdapter
        }
    }

    override suspend fun asyncOnStart() {
        noteListController.start()
    }

    // endregion

    // region private methods
    private fun handleNoteListChanged(noteList: MutableList<NoteListItemViewModel>) {
        noteListItemAdapter.items = noteList
    }

    private fun handleControllerEvents(noteListEvent: NoteListEvent) {
        when (noteListEvent) {
            is NoteListEvent.CreateNote -> {
                val action = NoteListFragmentDirections.openNoteEditor()
                view?.findNavController()?.navigate(action)
            }
            is NoteListEvent.EditNote -> {
                val action = NoteListFragmentDirections.openNoteEditor(noteListEvent.noteId)
                view?.findNavController()?.navigate(action)
            }
        }
    }

    private fun handleItemEvents(noteListItemEvent: NoteListItemEvent) {
        when (noteListItemEvent) {
            is NoteListItemEvent.NoteSelectedEvent -> {
                noteListController.onNoteSelected(noteListItemEvent.note)
            }
        }
    }
    // endregion
}
