package com.aguragorn.notes.notelist.presentation.model

import com.aguragorn.common.livedata.Event

sealed class NoteListItemEvent : Event {
    class NoteSelectedEvent(val note: NoteListItemViewModel) : NoteListItemEvent()
}