package com.aguragorn.notes.notelist.presentation

import androidx.lifecycle.ViewModel
import com.aguragorn.common.async.TaskQueue
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.execute
import com.aguragorn.common.error.messageOrClassName
import com.aguragorn.common.livedata.*
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.note.domain.usecase.GetAllNotesUseCase
import com.aguragorn.notes.notelist.presentation.model.NoteListEvent
import com.aguragorn.notes.notelist.presentation.model.NoteListItemViewModel
import com.aguragorn.notes.notelist.presentation.model.from
import com.aguragorn.notes.notesharing.domain.usecase.GetNotesSharedToMeUseCase

class NoteListController(
    private val getAllNotesUseCase: GetAllNotesUseCase = Provider.singleton(),
    private val getNotesSharedToMeUseCase: GetNotesSharedToMeUseCase = Provider.singleton()
) : ViewModel() {
    val noteList: MutableLiveList<NoteListItemViewModel> = mutableLiveListOf()
    val event = mutableLiveDataOf<NoteListEvent>()

    suspend fun start() {
        loadNotes()
        getNotesSharedToMeUseCase.execute().await()
        loadNotes()
    }

    fun onNewNoteSelected() {
        TaskQueue.runUI { event.value = NoteListEvent.CreateNote }
    }

    fun onNoteSelected(note: NoteListItemViewModel) {
        TaskQueue.runUI { event.value = NoteListEvent.EditNote(note.id) }
    }

    private fun loadNotes() {
        getAllNotesUseCase.execute()
            .onSuccess(TaskQueue.Main) { response ->
                val notes = response.notes
                noteList.clear()
                noteList.addAll(notes.map { NoteListItemViewModel.from(it) })
            }.onFailure {
                Log.e("TAG", it.messageOrClassName, it)
            }
    }
}
