package com.aguragorn.notes.notelist.presentation.model

import com.aguragorn.common.livedata.Event

sealed class NoteListEvent : Event {
    object CreateNote : NoteListEvent()
    class EditNote(val noteId: String) : NoteListEvent()
}