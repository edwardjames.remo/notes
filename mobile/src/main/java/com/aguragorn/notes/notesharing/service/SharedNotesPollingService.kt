package com.aguragorn.notes.notesharing.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import com.aguragorn.common.di.Provider
import com.aguragorn.common.logging.Log
import com.aguragorn.common.utils.alarmManager
import com.aguragorn.notes.JOB_ID_SHARED_NOTES_POLLING
import com.aguragorn.notes.notesharing.utils.TAG_NOTE_SHARING
import kotlinx.coroutines.runBlocking

class SharedNotesPollingService : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) = runBlocking {
        Log.d(TAG_NOTE_SHARING, "launching shared notes polling.")
        SharedNotesPollingController.start()
        schedule()
    }

    companion object {
        private val context by lazy { Provider.singleton<Context>() }
        private val pollingIntent by lazy {
            PendingIntent.getBroadcast(
                context, JOB_ID_SHARED_NOTES_POLLING,
                Intent(context, SharedNotesPollingService::class.java), 0
            )
        }

        fun schedule() {
            try {
                val nextSyncTime = SystemClock.elapsedRealtime() + (60 * 1000)

                Log.d(TAG_NOTE_SHARING, "Scheduling shared notes polling")
                context.alarmManager.setExact(AlarmManager.ELAPSED_REALTIME, nextSyncTime, pollingIntent)
            } catch (e: Throwable) {
                Log.e(TAG_NOTE_SHARING, "Scheduling shared notes polling failed", e)
            }
        }

        fun cancel() {
            context.alarmManager.cancel(pollingIntent)
        }
    }
}