package com.aguragorn.notes.notesharing.service

import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.execute
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.notesharing.domain.usecase.GetNotesSharedToMeUseCase
import com.aguragorn.notes.notesharing.utils.TAG_NOTE_SHARING
import com.aguragorn.notes.preferences.SessionPreferences

object SharedNotesPollingController {
    // use a getter so that the Provider will not be called prematurely
    private val sessionPreferences by lazy { Provider.singleton<SessionPreferences>() }
    private val getNotesSharedToMeUseCase by lazy { Provider.singleton<GetNotesSharedToMeUseCase>() }

    suspend fun start() {
        try {
            Log.d(TAG_NOTE_SHARING, "retrieving notes shared to ${sessionPreferences.loggedInUserEmail}.")
            getNotesSharedToMeUseCase.execute().await()
        } catch (e: Throwable) {
            Log.e(TAG_NOTE_SHARING, "retrieving notes shared to ${sessionPreferences.loggedInUserEmail} failed.", e)
        }
    }
}