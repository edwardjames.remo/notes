package com.aguragorn.notes.notesharing.data.source

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.Promise
import com.aguragorn.common.async.resolve
import com.aguragorn.notes.notesharing.data.utils.toParseObject
import com.aguragorn.notes.notesharing.data.utils.getParseQuery
import com.aguragorn.notes.notesharing.data.utils.toSharedNote
import com.aguragorn.notes.notesharing.domain.model.SharedNote
import java.util.*

class SharedNoteParseDB : SharedNoteDataSource {
    override fun saveAsync(data: SharedNote): Promise<SharedNote> = Async<SharedNote>().execute {
        try {
            if (data.id == null) data.id = UUID.randomUUID().toString()
            val parseSharedNote = data.toParseObject()

            parseSharedNote.save()

            resolve(parseSharedNote.toSharedNote())
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    override fun getAllSharedToUserAsync(toUser: String): Promise<List<SharedNote>> = Async<List<SharedNote>>().execute {
        try {
            val parseSharedNotes = SharedNote.getParseQuery().apply {
                this.whereEqualTo(SharedNote::toUser.name, toUser)
            }.find()

            resolve(parseSharedNotes.map { it.toSharedNote() })
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

    override fun deleteAllAsync(data: List<SharedNote>?): Promise<Unit> = Async<Unit>().execute {
        try {
            val ids = data?.map { it.id } ?: run {
                resolve()
                return@execute
            }

            val parseSharedNotes = SharedNote.getParseQuery().apply {
                this.whereContainedIn(SharedNote::id.name, ids)
            }.find()

            parseSharedNotes.forEach { it.deleteEventually() }
            resolve()
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }
}