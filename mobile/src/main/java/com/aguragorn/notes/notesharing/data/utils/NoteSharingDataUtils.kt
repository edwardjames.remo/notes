package com.aguragorn.notes.notesharing.data.utils

import com.aguragorn.notes.notesharing.domain.model.SharedNote
import com.parse.ParseObject
import com.parse.ParseQuery

fun SharedNote.toParseObject(): ParseObject = ParseObject(SharedNote::class.java.simpleName).apply {
    put(SharedNote::id.name, id!!)
    put(SharedNote::title.name, title)
    put(SharedNote::description.name, description)
    put(SharedNote::fromUser.name, fromUser)
    put(SharedNote::toUser.name, toUser)
}

fun SharedNote.Companion.getParseQuery(): ParseQuery<ParseObject> =
    ParseQuery.getQuery<ParseObject>(SharedNote::class.java.simpleName)

fun ParseObject.toSharedNote(): SharedNote = SharedNote(
    id = getString(SharedNote::id.name),
    title = getString(SharedNote::title.name)!!,
    description = getString(SharedNote::description.name)!!,
    fromUser = getString(SharedNote::fromUser.name)!!,
    toUser = getString(SharedNote::toUser.name)!!
)