package com.aguragorn.notes

import android.app.Application
import com.aguragorn.common.di.Provider
import com.aguragorn.common.logging.Log
import com.aguragorn.common.logging.LogCatLogger
import com.aguragorn.common.logging.LogLevel
import com.aguragorn.notes.data.source.StorageContext
import com.aguragorn.notes.di.CoreDI
import com.aguragorn.notes.di.MobileDI
import com.aguragorn.notes.preferences.PreferenceManager
import com.parse.Parse

class NotesApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Log.apply {
            if (BuildConfig.DEBUG) {
                init(LogLevel.DEBUG)
                registerLogger(LogCatLogger)
            } else {
                init(LogLevel.INFO)
            }
        }

        CoreDI.setup(context = this)
        MobileDI.setup(context = this)

        Provider.singleton<StorageContext>().init()
        Provider.singleton<PreferenceManager>().init()

        Parse.initialize(
            Parse.Configuration.Builder(this)
                .applicationId("honorsoft-notes-db")
                .clientKey("Summit Pringles Fudgee Barr")
                .server("https://honorsoft-notes-db.herokuapp.com/parse/")
                .build()
        )
    }


}