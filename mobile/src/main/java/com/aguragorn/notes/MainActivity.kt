package com.aguragorn.notes

import android.os.Bundle
import com.aguragorn.common.async.AsyncActivity
import com.aguragorn.common.di.Provider
import com.aguragorn.common.domain.usecase.execute
import com.aguragorn.common.logging.Log
import com.aguragorn.notes.notesharing.service.SharedNotesPollingService
import com.aguragorn.notes.splashscreen.presentation.TAG_AUTHENTICATION
import com.aguragorn.notes.user.domain.usecase.GetLoggedInUserUseCase
import com.parse.ParseUser

class MainActivity : AsyncActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

    override suspend fun asyncOnDestroy() {
        val loggedInUser = Provider.singleton<GetLoggedInUserUseCase>().execute().await()
        Log.i(TAG_AUTHENTICATION, "logging-out: ${loggedInUser?.id}")
        ParseUser.logOutInBackground()
        SharedNotesPollingService.cancel()
    }
}
