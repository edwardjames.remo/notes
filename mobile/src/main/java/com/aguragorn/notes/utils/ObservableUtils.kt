package com.aguragorn.notes.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.aguragorn.common.utils.*

/**
 * Adds a [handler] that is aware of it's [owner]'s lifecycle. The
 * handler is automatically removed from the [Event]'s notification
 * system when the [owner] gets destroyed.
 */
fun <T : Event> Observable<T>.addLifecycleAwareHandler(owner: LifecycleOwner, handler: (T) -> Unit) {
    val lifecycleObserver = object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            removeHandler(owner)
            owner.lifecycle.removeObserver(this)
        }
    }

    owner.lifecycle.addObserver(lifecycleObserver)
    addHandler(owner, handler)
}

fun <T : Event> Observable<T>.addLifecycleAwareObserver(owner: LifecycleOwner, observer: Observer<T>) {
    val lifecycleObserver = object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            removeObserver(owner)
            owner.lifecycle.removeObserver(this)
        }
    }

    owner.lifecycle.addObserver(lifecycleObserver)
    addObserver(owner, observer)
}

typealias ObservableList<T> = Observable<MutableList<T>>

fun <T> ObservableList<T>.add(item: T) {
    val updatedItems = this.value ?: mutableListOf()
    if (updatedItems.add(item)) {
        this.value = updatedItems
    }
}

fun <T> ObservableList<T>.addAll(elements: Collection<T>) {
    val updatedItems = this.value ?: mutableListOf()
    if (updatedItems.addAll(elements)) {
        this.value = updatedItems
    }
}

fun <T> ObservableList<T>.clear() {
    val updatedItems = this.value?.toMutableList()
    if (updatedItems.isNullOrEmpty()) return
    updatedItems.clear()
    this.value = updatedItems
}

fun <T> ObservableList<T>.removeAll(elements: Collection<T>) {
    val updatedItems = this.value?.toMutableList() ?: mutableListOf()
    if (updatedItems.removeAll(elements)) {
        this.value = updatedItems
    }
}

fun <T> ObservableList<T>.set(index: Int, item: T) {
    val updatedItems = this.value?.toMutableList() ?: mutableListOf()
    if (updatedItems.set(index, item) != item) {
        this.value = updatedItems
    }
}

operator fun <T> ObservableList<T>.get(index: Int): T? {
    val innerList: MutableList<T> = this.value ?: return null

    if (index < 0 ||
        innerList.isEmpty() ||
        index > innerList.size
    ) return null

    return innerList[index]
}

fun <T> observableListOf(value: T? = null): ObservableList<T> {
    val liveList = ObservableList<T>()

    if (value != null) {
        liveList.value = mutableListOf(value)
    }

    return liveList
}

fun <T> Collection<T>.toObservableList() = ObservableList<T>().apply {
    value = toMutableList()
}