package com.aguragorn.notes.preferences

import android.content.Context
import com.aguragorn.common.di.Provider
import github.agustarc.koap.Koap

class PreferenceManager(
    private val context: Context = Provider.singleton()
) {
    fun init() {
        Koap.bind(context, MobileSessionPreferences)
    }
}