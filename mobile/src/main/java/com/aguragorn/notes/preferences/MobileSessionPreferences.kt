package com.aguragorn.notes.preferences

import github.agustarc.koap.PreferenceHolder
import github.agustarc.koap.delegator.ReadWriteString

object MobileSessionPreferences : SessionPreferences, PreferenceHolder("session") {
    override var loggedInUserId: String by ReadWriteString(default = "")
    override var loggedInUserEmail: String by ReadWriteString(default = "")
}