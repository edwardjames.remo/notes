package com.aguragorn.notes.user.data.source

import com.aguragorn.common.async.Async
import com.aguragorn.common.async.Promise
import com.aguragorn.notes.user.domain.model.User
import com.parse.ParseUser
import java.util.*

class UserParseDB: UserDataSource {
    override fun getLoggedInUser(): Promise<User?> = Async<User?>().execute {
        try {
            val user = ParseUser.getCurrentUser()?.let {
                val id = it.getString(User::id.name) ?: UUID.randomUUID().toString()
                it.put(User::id.name, id)
                it.saveEventually()
                User(
                    id = id,
                    username = it.username,
                    email = it.email
                )
            }

            resolve(user)
        } catch (e: Throwable) {
            rejectWith(e)
        }
    }

}