package com.aguragorn.common.di

import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test

@Suppress("FunctionName")
class ProviderTests {

    @Before
    fun setup() {
        Provider.register { FakeClass() as Fake }
    }

    @Test
    fun `newInstance() should return an instance when the type is registered`() {
        try {
            @Suppress("UNUSED_VARIABLE")
            val fake: Fake = Provider.newInstance()

            assertTrue(true)
        } catch (e: Exception) {
            fail("Fake should be injected properly")
        }
    }

    @Test
    fun `newInstance() should return different instance per call`() {
        val fake1: Fake = Provider.newInstance()
        val fake2: Fake = Provider.newInstance()

        assertTrue("newInstance returned the same instance", fake1 != fake2)
    }

    @Test
    fun `singleton() should return an instance when the type is registered`() {
        try {
            @Suppress("UNUSED_VARIABLE")
            val fake1: Fake = Provider.singleton()

            assertTrue(true)
        } catch (e: Exception) {
            fail("Fake should be injected properly")
        }
    }


    @Test
    fun `singleton() should return same instance per call`() {
        val fake1: Fake = Provider.singleton()
        val fake2: Fake = Provider.singleton()

        @Suppress("ReplaceAssertBooleanWithAssertEquality")
        assertTrue("singleton returned different instances", fake1 == fake2)
    }

    @Test
    fun `singleton() and newInstance() should return different instances when singleton() is called first`() {
        val fake1: Fake = Provider.singleton()
        val fake2: Fake = Provider.newInstance()

        assertTrue("singleton() and newInstance() returned same instance", fake1 != fake2)
    }

    @Test
    fun `singleton() and newInstance() should return different instances when newInstance() is called first`() {
        val fake1: Fake = Provider.newInstance()
        val fake2: Fake = Provider.singleton()

        assertTrue("singleton() and newInstance() returned same instance", fake1 != fake2)
    }

    interface Fake
    class FakeClass : Fake
}