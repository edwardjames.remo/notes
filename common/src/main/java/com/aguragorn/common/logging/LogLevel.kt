package com.aguragorn.common.logging

enum class LogLevel(name: String, value: Int) {
    DEBUG("debug", 0),
    INFO("info", 1),
    WARNING("warning", 2),
    ERROR("error", 3),
    WTF("what the f*ck is this?!", 4);

    companion object  {
        val default = INFO
    }
}