package com.aguragorn.common.async

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * An [AppCompatActivity] whose method executions do not block the main thread.
 */
@Suppress("RedundantSuspendModifier", "UNUSED_PARAMETER")
abstract class AsyncActivity : AppCompatActivity() {

    protected val uiScope by lazy {
        UiLifecycleScope().also { lifecycle.addObserver(it) }
    }

    /**
     * [AppCompatActivity.onPostCreate] is called before this method executes
     */
    open suspend fun asyncOnPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
    }

    /**
     * [AppCompatActivity.onStart] is called before this method executes
     */
    open suspend fun asyncOnStart() {
    }

    /**
     * [AppCompatActivity.onResume] is called before this method executes
     */
    open suspend fun asyncOnResume() {
    }

    /**
     * [AppCompatActivity.onRestart] is called before this method executes
     */
    open suspend fun asyncOnRestart() {
    }

    /**
     * [AppCompatActivity.onPause] is called after this method executes
     */
    open suspend fun asyncOnPause() {
    }

    /**
     * [AppCompatActivity.onStop] is called after this method executes
     */
    open suspend fun asyncOnStop() {
    }

    /**
     * [AppCompatActivity.onDestroy] is called after this method executes
     */
    open suspend fun asyncOnDestroy() {
    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        uiScope.launch(Dispatchers.Main) { asyncOnPostCreate(savedInstanceState, persistentState) }
    }

    override fun onStart() {
        super.onStart()
        uiScope.launch { asyncOnStart() }
    }

    override fun onResume() {
        super.onResume()
        uiScope.launch { asyncOnResume() }
    }

    override fun onRestart() {
        super.onRestart()
        uiScope.launch { asyncOnRestart() }
    }

    override fun onPause() = runBlocking {
        asyncOnPause()
        super.onPause()
    }

    override fun onStop() = runBlocking {
        asyncOnStop()
        super.onStop()
    }

    override fun onDestroy() = runBlocking {
        asyncOnDestroy()
        super.onDestroy()
    }
}