package com.aguragorn.common.async

import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * Container for callbacks when an [Async] is completed, whether resolved or rejected.
 */
class Promise<T> {
    internal var onSuccessCallback: (suspend (T) -> Unit)? = null
    internal var onFailureCallback: (suspend (Throwable) -> Unit)? = null
    internal var alwaysCallback: (suspend () -> Unit)? = null

    internal var onSuccessQueue: TaskQueue? = null
    internal var onFailureQueue: TaskQueue? = null
    internal var alwaysQueue: TaskQueue? = null

    /**
     * The action to do once the promise is successfully resolved.
     * Use this if you are expecting data to be returned.
     *
     * @param queue the task queue to run the action on. Use
     *   [TaskQueue.Main] if you are going make changes to the UI.
     *   Defaults to [TaskQueue.Default]
     */
    fun onSuccess(queue: TaskQueue = TaskQueue.Default, callback: (suspend (T) -> Unit)): Promise<T> {
        onSuccessQueue = queue
        onSuccessCallback = callback
        return this
    }

    /**
     * The action to do once the promise is rejected.
     *
     * @param queue the task queue to run the action on. Use
     *   [TaskQueue.Main] if you are going make changes to the UI.
     *   Defaults to [TaskQueue.Default]
     */
    fun onFailure(queue: TaskQueue = TaskQueue.Default, callback: suspend (Throwable) -> Unit): Promise<T> {
        onFailureQueue = queue
        onFailureCallback = callback
        return this
    }

    /**
     * The action to do after the promise completes, whether or
     * not it succeeds or fails.
     *
     * @param queue the task queue to run the action on. Use
     *   [TaskQueue.Main] if you are going make changes to the UI.
     *   Defaults to [TaskQueue.Default]
     */
    fun always(queue: TaskQueue = TaskQueue.Default, callback: suspend () -> Unit): Promise<T> {
        alwaysQueue = queue
        alwaysCallback = callback
        return this
    }

    /**
     * Blocks the thread and waits for the result instead of calling a callback.
     * Execution is continued in the calling thread.
     * @throws Throwable when the [Promise] is rejected.
     */
    suspend fun await(): T = suspendCoroutine { continuation ->
        onSuccess { continuation.resume(it) }
        onFailure { continuation.resumeWithException(it) }
    }
}