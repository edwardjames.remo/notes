package com.aguragorn.common.async

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

/**
 *
 * Created by eremo on 9/22/2017.
 */
abstract class TaskQueue {

    abstract fun execute(task: suspend () -> Unit): ErrorHandler

    companion object {
        val Main
            get() = object : TaskQueue() {
                override fun execute(task: suspend () -> Unit) = ErrorHandler().also {
                    CustomScope(Dispatchers.Main, it).launch { task() }
                }
            }

        val Default
            get() = object : TaskQueue() {
                override fun execute(task: suspend () -> Unit) = ErrorHandler().also {
                    CustomScope(Dispatchers.Default, it).launch { task() }
                }
            }

        val IO
            get() = object : TaskQueue() {
                override fun execute(task: suspend () -> Unit) = ErrorHandler().also {
                    CustomScope(Dispatchers.Default, it).launch(Dispatchers.IO) { task() }
                }
            }

        /**
         * Executes a task asynchronously in the main thread (UI Thread).
         * Use this if you want to make UI changes.
         */
        fun runUI(task: suspend () -> Unit) = Main.execute(task)

        /**
         * Executes a task asynchronously in another background thread.
         * Use this for general tasks that do not involve UI changes.
         */
        fun run(task: suspend () -> Unit) = Default.execute(task)

        /**
         * Executes a task asynchronously in the IO thread.
         * Use this if you want to make network or file system operations
         */
        fun runIO(task: suspend () -> Unit) = IO.execute(task)

        fun invoke(queue: TaskQueue?, task: (suspend () -> Unit)?) {
            if (queue != null) {
                queue.execute { task?.invoke() }
            } else {
                runBlocking { task?.invoke() }
            }
        }
    }

    class ErrorHandler {
        internal var catcher: ((Throwable) -> Unit)? = null
        infix fun catch(handler: (Throwable) -> Unit) {
            catcher = handler
        }
    }
}

private class CustomScope(dispatcher: CoroutineDispatcher, errorHandler: TaskQueue.ErrorHandler) : CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext = job + dispatcher + CoroutineExceptionHandler { _, e ->
        if (e is CancellationException) {
            job.cancel(cause = e)
        } else {
            errorHandler.catcher?.invoke(e)
        }
    }
}