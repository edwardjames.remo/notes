package com.aguragorn.common.async

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@Suppress("RedundantSuspendModifier")
abstract class AsyncFragment : Fragment() {

    protected val uiScope by lazy {
        UiLifecycleScope().also { lifecycle.addObserver(it) }
    }

    open suspend fun asyncOnViewCreated(view: View, savedInstanceState: Bundle?) {}

    /**
     * [Fragment.onStart] is called before this method executes
     */
    open suspend fun asyncOnStart() {
    }

    /**
     * [Fragment.onResume] is called before this method executes
     */
    open suspend fun asyncOnResume() {
    }

    /**
     * [Fragment.onPause] is called after this method executes
     */
    open suspend fun asyncOnPause() {
    }

    /**
     * [Fragment.onStop] is called after this method executes
     */
    open suspend fun asyncOnStop() {
    }

    open suspend fun onDestroyAsync() {}

    open suspend fun asyncOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiScope.launch(Dispatchers.Main) { asyncOnViewCreated(view, savedInstanceState) }
    }

    override fun onStart() {
        super.onStart()
        uiScope.launch { asyncOnStart() }
    }

    override fun onResume() {
        super.onResume()
        uiScope.launch { asyncOnResume() }
    }

    override fun onPause() = runBlocking {
        asyncOnPause()
        super.onPause()
    }

    override fun onStop() = runBlocking {
        asyncOnStop()
        super.onStop()
    }

    final override fun onDestroy() = runBlocking {
        onDestroyAsync()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        uiScope.launch { asyncOnActivityResult(requestCode, resultCode, data) }
    }
}