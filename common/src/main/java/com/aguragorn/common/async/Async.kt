package com.aguragorn.common.async

/**
 * A [Async] is primarily used to call code after a long running task finishes execution,
 * whether by failing or succeeding, without blocking the UI thread.
 *
 * Callbacks inside a [Async]'s [Promise] are called depending on whether the [Async]
 * is resolved or rejected.
 *
 * Created by eremo on 8/4/2017.
 */
class Async<Resp> {
    internal val promise = Promise<Resp>()

    /**
     * Performs a [task] in a background [Thread].
     *
     * @param task a lambda expression that is executed in a background [Thread]. In which
     *   the [Async] execution can be [resolve]d or [rejectWith]ed.
     * @return a [Promise] where you can set what to do when the [Async] execution is completed
     *   whether resolved or rejected.
     */
    fun execute(taskQueue: TaskQueue = TaskQueue.Default, task: suspend Async<Resp>.() -> Unit): Promise<Resp> {
        taskQueue.execute {
            this.task()
        } catch { e ->
            rejectWith(e)
        }

        return promise
    }

    /**
     * Resolves the promise. This must be called in the task passed to [Async.execute].
     * This will in turn call the action for successful promise.
     */
    fun resolve(data: Resp) {
        TaskQueue.invoke(this.promise.onSuccessQueue) { this.promise.onSuccessCallback?.invoke(data) }
        TaskQueue.invoke(this.promise.alwaysQueue, this.promise.alwaysCallback)
    }

    /**
     * Rejects the promise. This must be called in the task passed to [Async.execute].
     * This will in turn call the action for failed promise.
     */
    fun rejectWith(error: Throwable) {
        TaskQueue.invoke(this.promise.onFailureQueue) { promise.onFailureCallback?.invoke(error) }
        TaskQueue.invoke(this.promise.alwaysQueue, this.promise.alwaysCallback)
    }
}

fun Async<Unit>.resolve() {
    TaskQueue.invoke(this.promise.onSuccessQueue) { this.promise.onSuccessCallback?.invoke(Unit) }
    TaskQueue.invoke(this.promise.alwaysQueue, this.promise.alwaysCallback)
}