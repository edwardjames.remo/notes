package com.aguragorn.common.collection

fun <T> Collection<T>?.isSameAs(other: Collection<T>?): Boolean {
    if (this == null && other == null) return true
    // if only one is null the statement will be true since null is not equal to 0
    if (this?.size != other?.size) return false
    // at this point both are non-null
    if (this!!.containsAll(other!!)
        && other.containsAll(this)
    ) return true

    return false
}