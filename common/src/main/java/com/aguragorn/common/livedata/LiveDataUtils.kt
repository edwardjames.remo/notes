package com.aguragorn.common.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

typealias MutableLiveList<T> = MutableLiveData<MutableList<T>>

fun <T> MutableLiveList<T>.add(item: T) {
    val updatedItems = this.value ?: mutableListOf()
    if (updatedItems.add(item)) {
        this.value = updatedItems
    }
}

fun <T> MutableLiveList<T>.addAll(elements: Collection<T>) {
    val updatedItems = this.value ?: mutableListOf()
    if (updatedItems.addAll(elements)) {
        this.value = updatedItems
    }
}

fun <T> MutableLiveList<T>.clear() {
    val updatedItems = this.value
    if (updatedItems.isNullOrEmpty()) return
    updatedItems.clear()
    this.value = updatedItems
}

fun <T> MutableLiveList<T>.removeAll(elements: Collection<T>) {
    val updatedItems = this.value ?: mutableListOf()
    if (updatedItems.removeAll(elements)) {
        this.value = updatedItems
    }
}

fun <T> MutableLiveList<T>.set(index: Int, item: T) {
    val updatedItems = this.value ?: mutableListOf()
    if (updatedItems.set(index, item) != item) {
        this.value = updatedItems
    }
}

operator fun <T> MutableLiveList<T>.get(index: Int): T? {
    var innerList = this.value

    if (innerList == null) {
        innerList = mutableListOf()
        this.value = innerList
    }

    if (index < 0 ||
        innerList.isEmpty() ||
        index > innerList.size
    ) return null

    return innerList[index]
}

fun <T> mutableLiveListOf(value: T? = null): MutableLiveList<T> {
    val liveList = MutableLiveList<T>()

    if (value != null) {
        liveList.value = mutableListOf(value)
    }

    return liveList
}

fun <T> mutableLiveDataOf(value: T? = null): MutableLiveData<T> {
    val liveData = MutableLiveData<T>()

    if (value != null) {
        liveData.value = value
    }

    return liveData
}

fun <T> liveDataOf(value: T? = null): LiveData<T> {
    val liveData = MutableLiveData<T>()

    if (value != null) {
        liveData.value = value
    }

    return liveData
}

fun <T> MutableLiveData<T>.copy() = mutableLiveDataOf(value)
fun <T> LiveData<T>.copy() = liveDataOf(value)

fun <T> Collection<T>.toMutableLiveList() = MutableLiveList<T>().apply {
    value = toMutableList()
}