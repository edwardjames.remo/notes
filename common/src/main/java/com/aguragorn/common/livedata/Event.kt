package com.aguragorn.common.livedata

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

interface Event

fun <T : Event> MutableLiveData<T>.addHandler(owner: LifecycleOwner, handler: (T) -> Unit) {
    observe(owner, Observer {
        if (it != null) {
            handler(it)
            value = null
        }
    })
}