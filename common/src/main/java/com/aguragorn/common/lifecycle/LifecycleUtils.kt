package com.aguragorn.common.lifecycle

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.*


/**
 * Created by eremo on 2/5/18.
 */

fun <T : ViewModel> AppCompatActivity.getViewModel(
    modelClass: Class<T>,
    viewModelFactory: ViewModelProvider.Factory? = null
): T {
    return viewModelFactory?.let {
        ViewModelProviders.of(this, it).get(modelClass)
    } ?: ViewModelProviders.of(this).get(modelClass)
}

inline fun <reified T : ViewModel> AppCompatActivity.getViewModel(
    viewModelFactory: ViewModelProvider.Factory? = null
): T {
    return viewModelFactory?.let {
        ViewModelProviders.of(this, it).get(T::class.java)
    } ?: ViewModelProviders.of(this).get(T::class.java)
}

fun <T : ViewModel> Fragment.getViewModel(
    modelClass: Class<T>,
    viewModelFactory: ViewModelProvider.Factory? = null
): T {
    return viewModelFactory?.let {
        ViewModelProviders.of(activity!!, it).get(modelClass)
    } ?: ViewModelProviders.of(activity!!).get(modelClass)
}

inline fun <reified T : ViewModel> Fragment.getViewModel(
    viewModelFactory: ViewModelProvider.Factory? = null
): T {
    return viewModelFactory?.let {
        ViewModelProviders.of(activity!!, it).get(T::class.java)
    } ?: ViewModelProviders.of(activity!!).get(T::class.java)
}

fun <T : ViewDataBinding> AppCompatActivity.setContentView(@LayoutRes layoutId: Int): T {
    val binding = DataBindingUtil.setContentView<T>(this, layoutId)
    binding.lifecycleOwner = this
    return binding
}

/**
 * The caller of this method is the one who needs to set the lifeCycleOwner of the binding.
 */
fun <T : ViewDataBinding> ViewGroup.inflate(
    @LayoutRes layoutRes: Int,
    attachToRoot: Boolean = false
): T {
    return DataBindingUtil.inflate(LayoutInflater.from(context), layoutRes, this, attachToRoot)
}

fun <T : ViewDataBinding> Fragment.inflate(
    inflater: LayoutInflater,
    @LayoutRes
    layoutId: Int,
    container: ViewGroup?,
    attachToRoot: Boolean = false
): T {
    val binding = DataBindingUtil.inflate<T>(inflater, layoutId, container, attachToRoot)
    binding.lifecycleOwner = this
    return binding
}

