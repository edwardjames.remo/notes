package com.aguragorn.common.domain.usecase

import com.aguragorn.common.async.Promise

interface UseCase<Req, Resp> {
    fun execute(request: Req): Resp
}

typealias AsyncUseCase<Req, Resp> =  UseCase<Req, Promise<Resp>>
typealias UseCaseNoPayload<Resp> = UseCase<Unit, Resp>
typealias AsyncUseCaseNoPayload<Resp> = UseCaseNoPayload<Promise<Resp>>


fun <Resp> UseCaseNoPayload<Resp>.execute() = execute(Unit)
