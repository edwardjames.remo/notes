package com.aguragorn.common.utils

class NotImplementedException(msg: String? = null) : Exception(msg)