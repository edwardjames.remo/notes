package com.aguragorn.common.utils

import android.app.AlarmManager
import android.app.job.JobScheduler
import android.content.Context

val Context.alarmManager get() = getSystemService(Context.ALARM_SERVICE) as AlarmManager
val Context.jobScheduler get() = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler