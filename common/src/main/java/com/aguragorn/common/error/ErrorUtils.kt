package com.aguragorn.common.error

val Throwable.messageOrClassName get() = message ?: this::class.java.name